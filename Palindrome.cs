using System;
class Palindrome{
	public static void Main(String[] args){
		Console.WriteLine("Enter a number: ");
		int a;
		Int32.TryParse(Console.ReadLine(),out a);
		string flag = IsPalindrome(a);
		Console.WriteLine("Palindrome Number: {0}",flag);
		Console.Read();
	}
	
	public static string IsPalindrome(int number)
	{
		if((number>10)&&(number<10000))
		{
			int b=number, temp, c=0;
			while(b!=0)
			{
				temp=b%10;
				c=c*10+temp;
				b=b/10;
			}
			if(c==number)
				return "yes";
			return "No";
		}
		return "invalid";
	}
}