using System;
class PrintBinaryNumber{
	public static void Main(string[] args){
		int a,output;
		Console.WriteLine("Enter a number: ");
		Int32.TryParse(Console.ReadLine(),out a);
		output = PrintBinary(a);
		Console.WriteLine("Binary Number: {0}",output);
		Console.Read();
	}
	public static int PrintBinary(int n)
	{
		if(n>0)
		{
			int b=n, temp=1,output=0,c;
			while(b!=0)
			{
				temp=temp*10+(b%2);
				b=b/2;
			}c=temp;
			while(temp!=0)
			{
				output=output*10+temp%10;
				temp=temp/10;
			}
			output=output/10;
			return output;
		}
		return -1;
	}
}