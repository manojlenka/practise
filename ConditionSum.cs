using System;
class ConditionSum{
	public static void Main(String[] args){
		Console.WriteLine("Enter the array: ");
		string[] str=(Console.ReadLine()).Split(' ');
		int[] a = Array.ConvertAll<string,int>(str,int.Parse);
		int b;
		Console.WriteLine("Enter number to which you want to divide:");
		Int32.TryParse(Console.ReadLine(), out b);
		int result = FindSum(a,b);
		Console.WriteLine("Total match found: {0}",result);
		Console.Read();
	}
	public static int FindSum(int[] arr,int condition ){
		int sum=0;
		for(int i=0;i<arr.Length;i++)
		{
			if(arr[i]%condition==0)
			{
				sum=sum+arr[i];
			}
		}
		if(sum>0){
			return sum;
		}
		else{
			return -1;
		} 
	}
}