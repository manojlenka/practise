class AtmMoneyOut{
	public static void Main(string[] s)
	{
		int rupee;
		int a=0,b=0;
		System.Console.WriteLine("Enter the amount: ");
		System.Int32.TryParse(System.Console.ReadLine(), out rupee);
		if((rupee%100)!=0)
		{
			System.Console.WriteLine("Requested amount cannot be taken out.");
		}
		else
		{
			if((rupee/500)>0)
			{
				a=rupee/500;
				rupee-=(a*500);
			}
			if((rupee/100)>0)
			{
				b=rupee/100;
				rupee-=(b*100);
			}
		}
		System.Console.WriteLine("Total 500 notes: {0} and 100 notes: {1}",a,b);
		Console.Read();
	}
}